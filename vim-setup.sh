#!/bin/bash

# ultimate vim configuration
# https://github.com/amix/vimrc

PLUGINS=~/.vim_runtime/vimrcs/plugins_config.vim

git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

# I like trees on the right side
sed -i 's/let g:NERDTreeWinPos = "left"/let g:NERDTreeWinPos = "right"/' ${PLUGINS}
sed -i 's/let NERDTreeShowHidden=0/let NERDTreeShowHidden=1/' ${PLUGINS}
sed -i "s/'__pycache__'/'__pycache__', '.git'/" ${PLUGINS}

# set up tmux
mkdir ~/.tmux
cp ./tmux.conf ~/
cp ./tmux/dev ~/.tmux/dev

